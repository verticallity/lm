ALL		:= $(shell ls -t *.c *.cpp 2>/dev/null |sed -e 's/\.c\(pp\)\?$$//g')
NEWEST		:= $(shell echo $(ALL) |tr ' ' '\n' |head -1)

CFLAGS		= -Wall -O2 -g
LDFLAGS		= -lncurses -pthread -lpanel

CC		= gcc
CCFLAGS		= $(CFLAGS) -std=c99

CXX		= g++
CXXFLAGS	= $(CFLAGS) -std=c++0x

default: $(NEWEST)

%.o: %.c
	$(CC) -c $(CCFLAGS) $^

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $^

%: %.o
	$(CXX) -o $@ $^ $(LDFLAGS)
clean:
	rm -f *.o

all: $(ALL)

.PRECIOUS: %.o
.SUFFIXES:
