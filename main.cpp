#include <chrono>
#include <ctime>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

#include <ncurses.h>

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>



// for some reason ncurses thinks that my backspace is 263? so this should work
#define REAL_BACKSPACE 127



std::vector<std::string> messages;
int width, height;

// literally just redrawing the entire screen since this isn't ever that large
void draw()
{
	// can't seem to get resizing to work so I'm just updating every time
	getmaxyx(stdscr, height, width);
	int at_y = (height - messages.size()) / 2;
	
	clear();
	for (unsigned i = 0; i < messages.size(); i++) {
		mvaddstr(at_y + i, (width - messages[i].size()) / 2,
			messages[i].c_str());
	}
}

int main()
{
	initscr();
	cbreak();
	noecho();
	
	char mode = 'w';
	while (mode != 'q') {
		switch (mode) {
		// [w]rite new message
		case 'w':
			messages.push_back("");
			break;
		// [e]dit previous message
		case 'e':
			// just keep writing to previous
			break;
		// [r]ewrite previous message
		case 'r':
			messages.back() = "";
			break;
		
		// shouldn't get here, if so just ask for another mode
		default:
			goto newmode;
		}
		
		
		
		draw();
		
		
		
		int curch;
		while ((curch = getch()) != '\n') {
			if (curch == REAL_BACKSPACE) {
				if (messages.back().size()) {
					messages.back().pop_back();
					draw();
				}
			} else if (curch < 256) {
				messages.back().push_back(curch);
				draw();
			}
		}
		
		
		
newmode:
		mvaddstr(height - 1, 0,
			"[q]uit | [w]rite | [e]dit | [r]ewrite : ");
		int tm = getch();
		while (tm != 'q' &&
		       tm != 'w' &&
		       tm != 'e' &&
		       tm != 'r') {
			tm = getch();
		}
		mode = tm;
	}
	
	
	
	endwin();
	
	
	
	// guess I'll just have to trust SO for this one
	//   since unistd is a bit out of my usual
	struct passwd * pw = getpwuid(getuid());
	std::string homedir = pw->pw_dir;
	
	// don't accidentally write to root
	if (homedir.empty()) return 0;
	
	
	std::fstream file;
	file.open(homedir + "/.left_messages", std::ios::app);
	
	auto time = std::chrono::system_clock::to_time_t(
		std::chrono::system_clock::now());
	
	
	file << std::put_time(std::localtime(&time), "%F %T") << "\n\n";
	
	for (auto str : messages) {
		file << str << '\n';
	}
	
	file << "\n\n";
	
	file.close();
	
	
	return 0;
}
